#  Makefile
#
#  Copyright 2014-2015 Karl Lindén <karl.j.linden@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

LATEX     ?= latex
LATEXARGS ?= -halt-on-error

texfiles := \
	uppgift1.tex \
	uppgift2.tex \
	uppgift3.tex \
	uppgift4.tex
dvifiles := $(texfiles:.tex=.dvi)
pdffiles := $(texfiles:.tex=.pdf)
psfiles  := $(texfiles:.tex=.ps)

texinputs := sidfot.tex

cleanfiles := $(dvifiles)
cleanfiles += $(pdffiles)
cleanfiles += $(psfiles)
cleanfiles += $(texfiles:.tex=.aux)
cleanfiles += $(texfiles:.tex=.log)
cleanfiles += $(texfiles:.tex=.out)

all: pdf


dvi: $(dvifiles)
pdf: $(pdffiles)
ps: $(psfiles)

clean:
	-rm -f $(cleanfiles)

.PHONY: all dvi pdf ps clean

.DELETE_ON_ERROR:

%.dvi %.aux %.log %.out: %.tex $(texinputs)
	$(LATEX) $(LATEXARGS) $<
	while grep -q 'Rerun to get ' $*.log ; do $(LATEX) $(LATEXARGS) $< ; done

%.ps: %.dvi
	dvips $< -o $@

%.pdf: %.ps
	ps2pdf $< $@
